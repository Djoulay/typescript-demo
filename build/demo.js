"use strict";
///////////////////DEFINIR TYPE///////////////////////
Object.defineProperty(exports, "__esModule", { value: true });
//utile pour déclarer des class pour pramètrer les propriétés attendue au iveau de l'objet
//fonction salut prenant en paramètre un tableau t
//pour chaque élément du tableau rajouter salut
//assignation au tableau ne recevant que des chaîne de caractère
//peut s'écrire aussi t: string[]
//renvoie un tableau de chaîne de caractères
//return le tableau out
// une fonction qui ne retourne rien mettre void
// => function salut(t:  Array<string>): void {....
function salut(t) {
    var out = [];
    for (var _i = 0, t_1 = t; _i < t_1.length; _i++) {
        var item = t_1[_i];
        out.push('Salut' + item);
    }
    return out;
}
salut(['ju', 'seb', 'toto']);
/////////////////////////////////////////////////////
var a = 456;
//fonction avec un paramètre de type number
//le retour sera un bouléen === 0
//on défini le type du callback comme étant un nombre pour utiliser la fonction par la suite
// void car il ne va rien retourner
/*function isPair(nombre: number, callback: (number) => void): boolean{
    return nombre % 2 === 0
}


//préciser un type callback
//fonction isPair en paramètre 2 avec une fonction a effectuer lorsque l'opération aura bien fonctionnée

isPair(2, function (reste) {
    console.log(reste) //il faut préciser le callback
})*/
//passer en second paramètre d'option : je 'attend à avoir un objet avec propriété a de type number et b de type string
//? devant b : paramètre optionnel
//nombre: number | string => type nombre ou chaine de caractères
//de ce fait mettre un parsint de nombre si c'est une chaine
//<string>nombre et <number>nombre (renforce le typage de la variable)
function isPair(nombre, options) {
    if (typeof nombre !== 'number') {
        nombre = parseInt(nombre, 10);
    }
    return nombre % 2 === 0;
}
isPair(2, { a: 2, b: 'toto' });
///////////////////////CREER CLASSE////////////////////////////
//définir sa proprité au préalable : element
//public : visibilité de la propriété
//private : pas accessible depuis l'extèrieur
//protected: accessible que par les enfants mais depuis l'extèrieur
//cela marche aussi pour les méthodes : on peut aussi définir qu'une méthode sera static se faisant directement au niveau de la classe
var Demo = /** @class */ (function () {
    function Demo() {
    }
    Object.defineProperty(Demo.prototype, "element", {
        get: function () {
            return this._element;
        },
        set: function (value) {
            this._element = value; //défini ce qui est passé en paramètre
        },
        enumerable: true,
        configurable: true
    });
    return Demo;
}());
var d = new Demo();
d.element = 'Salut'; //modifier l'élément
console.log(d.element);
//créer des accesseurs et des mutateurs GET SET
var Demo1 = /** @class */ (function () {
    function Demo1() {
    }
    return Demo1;
}());
var Demoo = /** @class */ (function () {
    function Demoo(options) {
        this.options = options;
    }
    return Demoo;
}());
var r = new Demoo({
    autoplay: true,
    x: 2,
    success: function (data) {
    }
});
//CREER des NAMESPACE
/*namespace Julie { //création d'un objet vide Julie

    let deymo = //cette variable ne sera appelée qu'à l'intérieur de ce namespace : permet d'avoir des morceaux de code isolés les uns des autres

    export class Deymo {//création de la classe Deymo sur l'objet Julie

        private options; //déclaration de variable

        constructor(options) { //on attend l'objet DemoOption en paramètre
            this.options = options
        }
    }

}

let b = new Julie.Deymo ({ //création d'une propriété dessus
    autoplay: true,
    x:2,
    success: function (data) {

    }
})
*/
//OU système de MODULE
var Deimo = /** @class */ (function () {
    function Deimo(options) {
        this.options = options;
    }
    return Deimo;
}());
exports.default = Deimo;
/*autre fonction intéressante :
Enums : définir un objet qui va contenir des clefs permettant de définir les sortes de constante


INCONVENIENT

Attention on ne peut inclure n'importe-quel fonctions ne librairie il faut expliquer à Typescript la fonction enregistrée sous NPM dans un autre fichier
Idem pour les variables globales Jquery : faire un npm i -g typings (pour jquery)
Typings permet de chercher des définitions :
- typings search jquery (sur terminal)
- typings install dt~jquery --global
- création d'un dossier typings contenant un fichier index.d.ts faisant référence au fichier de définition
- puis charger typings/index.d.ts dans files du fichier tsconfig.json
- relancer tsc --watch
- repartir dans index.ts et taper : declare let $: JQueryStatic
ou faire un npm i --save jquery puis faire un import * as $ from 'jquery"*/
